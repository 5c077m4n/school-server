<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('', function() {
	return response()->json(['response' => 'Great Success!']);
});

Route::middleware('auth:api')->get('/current-user', function(Request $request) {
    return $request->user();
});

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::apiResource('image-upload', 'UploadImageController');

Route::group(['middleware' => 'auth:api'], function() {
	Route::apiResources([
		'admins' => 'UserController',
		'students' => 'StudentController',
		'courses' => 'CourseController'
	]);
});
