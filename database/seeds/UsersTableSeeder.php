<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::truncate();
		$faker = \Faker\Factory::create();

		User::create([
			'name' => 'Superuser',
			'email' => 'su@email.com',
			'role' => 'owner',
			'image_url' => 'http://localhost:8000/default-images/user.jpg',
			'password' => Hash::make('123')
		]);
    }
}
