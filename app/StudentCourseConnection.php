<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentCourseConnection extends Model {
	protected $fillable = ['course_id', 'student_id'];
}
