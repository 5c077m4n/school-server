<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;

class Student extends Model {
	protected $fillable = ['name', 'phone', 'email', 'image_url'];
	protected $table = 'students';

    public function courses() {
		return $this->belongsToMany(Course::class, 'course_student', 'student_id', 'course_id')
			->withTimestamps();
    }
}
