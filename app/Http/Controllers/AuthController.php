<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

class AuthController extends Controller {
	public function register(Request $request) {
		$user = User::create([
			'name' => $request->name,
			'role' => $request->role,
			'phone' => $request->phone,
			'email' => $request->email,
			'image_url' => $request->imageURL,
			'password' => bcrypt($request->password),
		]);
		$token = auth()->login($user);
		return $this->respondWithToken($token);
    }

    public function login(Request $request) {
		$credentials = $request->only(['email', 'password']);
		if(!$token = auth()->attempt($credentials))
			return response()->json(['error' => 'Unauthorized'], 401);
		else return $this->respondWithToken($token);
    }

    protected function respondWithToken($token) {
		return response()->json([
			'access_token' => $token,
			'token_type' => 'bearer',
			'expires_in' => auth()->factory()->getTTL()
		]);
	}
}
