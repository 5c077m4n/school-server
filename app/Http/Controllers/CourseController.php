<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Http\Resources\CourseResource;

class CourseController extends Controller {
	public function __construct() {
		$this->middleware('auth:api')->except(['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return Course::with('students')->get();
        // return CourseResource::collection(Course::with('students')->paginate(100));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $course = Course::create([
			'name' => $request->name,
			'description' => $request->description,
			'image_url' => ($request->image_url)?
				$request->image_url : 'http://localhost:8000/default-images/course.png'
		]);
		$course->students()->sync($request->students);
		return new CourseResource($course);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course) {
		return Course::where('id', '=', $course->id)
			->with('students')
			->first();
        // return new CourseResource($course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course) {
        $course->update($request->all());
		return new CourseResource($course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course) {
        Course::where('id', '=', $course->id)->delete();
		return response()->json([
			'success' => true
		], 200);
    }
}
