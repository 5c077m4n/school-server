<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UploadImageController extends Controller {
	public function __invoke(Request $request) {
		return $this->store($request);
	}

	// public function update(Request $request) {
	// 	if($request->hasFile('image')) {
	// 		$this->validate(
	// 			$request, ['image' => 'required|image|mimes:jpg,jpeg,png,jpg,gif,svg|min:1|max:4096']
	// 		);
	// 		$path = $request->file('image')->store('/uploads/images');
	// 		return response()->json([
	// 			'success' => true,
	// 			'link' => 'http://localhost:8000/'.$path
	// 		], 201);
	// 	}
	// 	return response()->json([
	// 		'success' => false,
	// 		'error' => 'No image found.'
	// 	], 400);
	// }

	public function store(Request $request) {
		if($request->hasFile('image')) {
			$this->validate(
				$request, ['image' => 'required|image|mimes:jpg,jpeg,png,jpg,gif,svg|min:1|max:4096']
			);
			$image = $request->file('image');
			$name = bin2hex(random_bytes(16)).'.'.$image->getClientOriginalExtension();
			$image->move(public_path('uploads/images'), $name);
			return response()->json([
				'success' => true,
				'link' => 'http://localhost:8000/uploads/images/'.$name
			], 201);
		}
		return response()->json([
			'success' => false,
			'error' => 'No image found.'
		], 400);
	}
}
