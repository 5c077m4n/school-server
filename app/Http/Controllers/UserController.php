<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserModel;
use App\Http\Resources\UserResource;

class UserController extends Controller {
	public function __construct() {
		$this->middleware('auth:api')->except(['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return User::all();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $user = User::create([
			'name' => $request->name,
			'role' => $request->role,
			'phone' => $request->phone,
			'email' => $request->email,
			'image_url' => ($request->imageURL)?
				$request->imageURL : 'http://localhost:8000/default-images/user.jpg',
			'password' => bcrypt($request->password),
		]);
		return new UserResource($user);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		return User::find($id)
			->first();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		User::find($id)
			->update($request->all());
		return new UserResource($user);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
		User::destroy($id);
		return response()->json([
			'success' => true
		], 200);
    }
}
