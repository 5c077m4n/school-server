<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Http\Resources\StudentResource;

class StudentController extends Controller {
	public function __construct() {
		$this->middleware('auth:api')->except(['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return Student::with('courses')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $student = Student::create([
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'image_url' => ($request->image_url)?
				$request->image_url : 'http://localhost:8000/default-images/user.jpg'
		]);
		$student->courses()->sync($request->courses);
		return new StudentResource($student);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student) {
		return Student::where('id', '=', $student->id)
			->with('courses')
			->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student) {
		$student->update($request->all());
		$student->courses()->sync($request->courses);
		return new StudentResource($student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student) {
		Student::where('id', '=', $student->id)->delete();
		return response()->json([
			'success' => true
		], 200);
    }
}
