<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student;

class Course extends Model {
	protected $fillable = ['name', 'description', 'image_url'];
	protected $table = 'courses';

    public function students() {
		return $this->belongsToMany(Student::class, 'course_student', 'course_id', 'student_id')
			->withTimestamps();
    }
}
